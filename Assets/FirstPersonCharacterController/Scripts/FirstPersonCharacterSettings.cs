using UnityEngine;

namespace FirstPersonCharacterController
{
	[CreateAssetMenu(menuName = "Data/First Person Character Controller/First Person Character Settings")]
	public class FirstPersonCharacterSettings : ScriptableObject
	{
		[Header("Movement")]
		public float mouseSensitivityX = 1f;
		public float mouseSensitivityY = 1f;
		public float moveSpeed = 1f;
		public float airDrag = 0f;
		public float groundDrag = 1f;
		public float railMountSpeed = 8.0f;

		[Header("Jump and gravity")]
		public float gravity = -9.81f;
		public float jumpHeight = 1f;
		
		[Header("Camera movement")]
		public float strafeRotation = 1f;
		public float cameraBobbingAmplitude = 0.08f;
		public float cameraBobbingFrequency = 2f;
		public AnimationCurve fallMovement;
		public float maxVelocity = 8f;
	}
}