using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FirstPersonCharacterController
{
    public class PlayerHand : MonoBehaviour
    {
        [SerializeField] private string inHandLayerName;
        [SerializeField] private PlayerInput playerInput;
        [SerializeField] private CursorController cursorController;

        public HoldableItem HeldItem { get; private set; }

        public void TryPickUp(HoldableItem newItem)
        {
            if (HeldItem != null) HeldItem.Drop();
            HeldItem = newItem;
            newItem.PickUp(inHandLayerName);
        }

        private void Update()
        {
            if (playerInput.GetButtonDown(PlayerInput.Action.Use) && !cursorController.IsSelecting && HeldItem != null)
            {
                HeldItem.Drop();
                HeldItem = null;
            }
        }

    }
}
