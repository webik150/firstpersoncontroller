using UnityEngine;

namespace FirstPersonCharacterController
{
	[CreateAssetMenu(menuName = "Data/First Person Character Controller/Cursor Manager")]
	public class CursorManager : ScriptableObject
	{
		public Color selectionColor;
		public float selectionScale = 1.5f;
		public float interactionDistance = 1.5f;

		public CursorController CursorController { get; private set; }

		public void RegisterCameraCursorController(CursorController cursorController)
		{
			CursorController = cursorController;
		}
	}
}