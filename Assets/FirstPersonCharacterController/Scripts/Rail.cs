using UnityEngine;

namespace FirstPersonCharacterController
{
    public class Rail : MonoBehaviour
    {

	    [SerializeField]private Transform lineStart;
	    [SerializeField]private Transform lineEnd;

        public Vector3 railPointA
        {
	        get => lineStart.position;
        }
        public Vector3 railPointB
        {
	        get => lineEnd.position;
        }
    }
}
