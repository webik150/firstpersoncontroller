using UnityEngine;

namespace FirstPersonCharacterController
{
	[RequireComponent(typeof(CharacterController), typeof(FirstPersonCharacterMovement), typeof(FirstPersonCharacterGroundCheck))]
	[RequireComponent(typeof(FirstPersonCharacterLook)), RequireComponent(typeof(PlayerInput))]
	public class FirstPersonRailHandler : MonoBehaviour
	{
		private CharacterController controller;
		private FirstPersonCharacterMovement player;
		private FirstPersonCharacterLook look;
		private FirstPersonCharacterGroundCheck groundCheck;
		private PlayerInput playerInput;
		[SerializeField] private FirstPersonCharacterSettings characterSettings;
		public LayerMask RailMask;
		[SerializeField] private Rail mountedRail;
		private Vector3 playerVelocity;
		private Vector3 movementDir;
		private Collider[] railColliders = new Collider[32];
		private Vector3 closestPoint = Vector3.zero;
		private bool pressedJump;
		private float characterRadius;
		
		/// <summary>
		/// The currently mounted rail. Null if unmounted.
		/// </summary>
		public Rail MountedRail
		{
			get => mountedRail;
		}

		// Start is called before the first frame update
		void Awake()
		{
			controller = GetComponent<CharacterController>();
			groundCheck = GetComponent<FirstPersonCharacterGroundCheck>();
			player = GetComponent<FirstPersonCharacterMovement>();
			playerInput = GetComponent<PlayerInput>();
			look = GetComponent<FirstPersonCharacterLook>();
			player.OnJump.AddListener(OnJump);
		}

		/// <summary>
		/// Event called when the player jumps.
		/// </summary>
		private void OnJump()
		{
			if (mountedRail)
			{
				movementDir = Vector3.Project(playerVelocity, movementDir).normalized;
				DismountRail();
			}
		}


		// Update is called once per frame
		void Update()
		{
			//Check needed to see if the Jump button was pressed while in air or if it was used to jump.
			if (groundCheck.IsGrounded)
			{
				pressedJump = false;
			}
			else if (playerInput.GetButtonDown(PlayerInput.Action.Jump))
			{
				pressedJump = true;
			}

			//Check if the player is inside the range of any rail
			if (!mountedRail && !groundCheck.IsGrounded && pressedJump && playerInput.GetButton(PlayerInput.Action.Jump))
			{
				int hitCount = groundCheck.GetCollidersNonAloc(RailMask, ref railColliders, true);
				Rail foundRail = null;
				for (int i = 0; i < hitCount; i++)
				{
					if (railColliders[i] != null)
					{
						foundRail = railColliders[i].gameObject.GetComponent<Rail>();
					}
					//TODO: Handle case when multiple rails overlap (but only if necessary)
					railColliders[i] = null;
				}

				MountRail(foundRail);
			}

			//Dismount if player stops holding jump
			if (mountedRail && !playerInput.GetButton(PlayerInput.Action.Jump))
			{
				player.Jump();
			}

			//Handle rail riding
			if (mountedRail)
			{
				player.ExternalVelocity = Vector3.Project(playerVelocity, movementDir).normalized * playerVelocity.magnitude;

				//Pull player towards the middle of the rail
				//First get the closest position on the rail
				closestPoint = mountedRail.railPointA + Vector3.Project(transform.position - mountedRail.railPointA, (mountedRail.railPointB - mountedRail.railPointA).normalized);
				//Move towards it
				transform.position = Vector3.Lerp(transform.position, closestPoint, Time.deltaTime * characterSettings.railMountSpeed);

				//Figure out if the player left the rail
				float distBetweenPoints = Vector3.Distance(mountedRail.railPointA, mountedRail.railPointB);
				if (Vector3.Distance(mountedRail.railPointA, closestPoint) > distBetweenPoints ||
					Vector3.Distance(mountedRail.railPointB, closestPoint) > distBetweenPoints)
				{
					//Player left the rail
					DismountRail();
				}

				// Dismount if not moving
				if (playerVelocity.sqrMagnitude < 1f)
				{
					DismountRail();
				}
			}

		}


		/// <summary>
		/// Mounts a player on a rail. Does nothing if supplied rail is null. Won't work properly if the player isn't actually in the range of the rail.
		/// </summary>
		/// <param name="rail"></param>
		private void MountRail(Rail rail)
		{
			if (rail == null) return;
			// Get current player velocity, but only on the X/Z plane.
			playerVelocity = controller.velocity - new Vector3(0, controller.velocity.y, 0);

			// Reset the velocity to apply to player each frame
			player.ExternalVelocity = Vector3.zero;

			// Get the direction of movement (albeit positive or negative)
			movementDir = rail.transform.forward;

			mountedRail = rail;

			// Take over player control
			look.HeadBobbing = false;
			player.UseExternalMovement = true;
			controller.detectCollisions = false;
			characterRadius = controller.radius;
			controller.radius = 0;
		}

		/// <summary>
		/// Dismount from current rail
		/// </summary>
		private void DismountRail()
		{
			look.HeadBobbing = true;
			player.UseExternalMovement = false;
			mountedRail = null;
			pressedJump = false;
			controller.detectCollisions = true;
			controller.radius = characterRadius;
		}

		void OnDrawGizmosSelected()
		{
			if (mountedRail)
			{
				//The rail itself
				Gizmos.color = Color.green;
				Gizmos.DrawLine(mountedRail.railPointA, mountedRail.railPointB);
				//Closest point to player on the rail
				Gizmos.color = Color.blue;
				var pr = closestPoint;
				Gizmos.DrawWireSphere(pr, 0.3f);
			}
		}

	}
}
