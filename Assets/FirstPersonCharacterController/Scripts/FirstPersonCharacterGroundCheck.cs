using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;

namespace FirstPersonCharacterController
{
    public class FirstPersonCharacterGroundCheck : MonoBehaviour
    {
        [SerializeField] private Transform groundCheck;
        [SerializeField] private Transform ceilingCheck;
        [SerializeField] private CharacterController characterController;
        [SerializeField] private LayerMask groundMask;
        private bool isGrounded = true;
        private bool isTouchingCeiling;
        private bool wasGrounded = true;
        public UnityEvent OnLand = new UnityEvent();

        /// <summary>
        /// Is the player grounded?
        /// </summary>
        public bool IsGrounded
        {
	        get => isGrounded;
	        set
	        {
		        isGrounded = value;
	        }
        }

        /// <summary>
        /// Is the player touching the ceiling?
        /// </summary>
        public bool IsTouchingCeiling { get => isTouchingCeiling; }

        /// <summary>
        /// Was the player grounded last frame?
        /// </summary>
        public bool WasGrounded { get => wasGrounded; }

        /// <summary>
        /// Layers to be considered as ground.
        /// </summary>
        public LayerMask GroundMask { get => groundMask; }

        // Start is called before the first frame update
        void Awake()
        {
            Assert.IsNotNull(groundCheck);
            Assert.IsNotNull(ceilingCheck);
        }

        // Update is called once per frame
        void Update()
        {
	        ceilingCheck.transform.localPosition = new Vector3(0, characterController.height, 0);
	        isGrounded = CheckGround(groundMask);
            isTouchingCeiling = Physics.CheckSphere(ceilingCheck.position, 0.2f, groundMask);
        }

        private void LateUpdate()
        {
	        if (!wasGrounded && isGrounded)
	        {
		        OnLand.Invoke();
	        }
            wasGrounded = isGrounded;
        }

        /// <summary>
        /// Check if player is above point
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        public bool IsAbovePoint(Vector3 point)
        {
	        return groundCheck.position.y+characterController.radius*0.5f >= point.y;
        }

        /// <summary>
        /// Check if player is standing on a specific LayerMask.
        /// </summary>
        /// <param name="mask"></param>
        /// <param name="triggers"></param>
        /// <returns>True if collider was found</returns>
        public bool CheckGround(LayerMask mask, bool triggers = false)
        {
            float radius = characterController.radius * characterController.transform.lossyScale.x;
            return Physics.CheckSphere(groundCheck.position + new Vector3(0, radius * 0.50f, 0), radius * 0.9f, mask, triggers? QueryTriggerInteraction.Collide:QueryTriggerInteraction.Ignore);
        }

        /// <summary>
        /// Finds all colliders the character is standing in/on.
        /// </summary>
        /// <param name="mask">Layer mask</param>
        /// <param name="colliders">Preallocated collider array</param>
        /// <param name="triggers">Should triggers be included</param>
        /// <returns></returns>
        public int GetCollidersNonAloc(LayerMask mask, ref Collider[] colliders, bool triggers = false)
        {
	        float radius = characterController.radius * characterController.transform.lossyScale.x;
	        return Physics.OverlapSphereNonAlloc(groundCheck.position + new Vector3(0, radius * 0.50f, 0), radius * 1.1f,colliders, mask, triggers ? QueryTriggerInteraction.Collide : QueryTriggerInteraction.Ignore);
        }

        private void OnDrawGizmosSelected()
        {
            float radius = characterController.radius * characterController.transform.lossyScale.x;
            if (isGrounded)
            {
                Gizmos.color = Color.green;
            }
            else
            {
                Gizmos.color = Color.red;
            }
            Gizmos.DrawWireSphere(groundCheck.position + new Vector3(0, radius*0.5f, 0),radius*0.9f);
        }
    }
}