﻿using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

namespace FirstPersonCharacterController
{
	public class FirstPersonCharacterLook : MonoBehaviour
	{
		[SerializeField] private PlayerInput playerInput;
		[SerializeField] private FirstPersonCharacterSettings characterSettings;
		[SerializeField] private CharacterController characterController;
		[SerializeField] private FirstPersonCharacterMovement characterMovement;
		[SerializeField] private Transform playerBody;
		[SerializeField] private Transform headTransform;
		[SerializeField] private Transform cameraTransform;
		[SerializeField] private Camera playerCamera;
		[SerializeField] private VolumeProfile volumeProfile;

		public bool HeadBobbing = true;

		private float xRotation;
		private float yaw;
		private float defaultFov;
		private MotionBlur motionBlur;
		private bool isHeadBobbing;
		private float startHeadBobbingTime;
		private float headBobPositionOffset;
		private float headFallPositionOffset;
		private float headBobYRotationOffset;

		private TweenerCore<float, float, FloatOptions> headBobEndTween;
		private TweenerCore<float, float, FloatOptions> fallTween;

		private void Awake()
		{
			Assert.IsNotNull(playerInput);
			Assert.IsNotNull(characterSettings);
			Assert.IsNotNull(characterController);
			Assert.IsNotNull(characterMovement);
			Assert.IsNotNull(playerBody);
			Assert.IsNotNull(headTransform);
			Assert.IsNotNull(cameraTransform);
			Assert.IsNotNull(playerCamera);
			Assert.IsNotNull(volumeProfile);

			Cursor.lockState = CursorLockMode.Locked;
			Cursor.visible = false;

			volumeProfile.TryGet(typeof(MotionBlur), out motionBlur);

			characterMovement.Fall += CharacterMovementOnFall;

			defaultFov = playerCamera.fieldOfView;
		}

		private void Update()
		{
			UpdateHead();
			UpdateZoom();
		}

		private void UpdateHead()
		{
			// Head position
			headTransform.localPosition = new Vector3(cameraTransform.localPosition.x, characterController.height - 0.2f, cameraTransform.localPosition.z);

			// Looking
			float mouseX = playerInput.GetAxis(PlayerInput.Action.LookHorizontal) * characterSettings.mouseSensitivityX * Time.deltaTime * 60f;
			yaw += mouseX;
			float mouseY = playerInput.GetAxis(PlayerInput.Action.LookVertical) * characterSettings.mouseSensitivityY * Time.deltaTime * 60f;
			xRotation += mouseY;
			xRotation = Mathf.Clamp(xRotation, -90f, 90f);

			// Strafe
			float targetZRotation = 0;
			float moveX = playerInput.GetAxis(PlayerInput.Action.MoveX);
			if (Mathf.Abs(moveX) > 0.1f)
				targetZRotation = -characterSettings.strafeRotation * Mathf.Sign(moveX);


			// Head bobbing
			var xzVelocity = characterController.velocity;
			xzVelocity.y = 0;
			var velocity = xzVelocity.magnitude;
			float headBobXRotationOffset = 0f;
			if (HeadBobbing)
			{
				if (Mathf.Abs(velocity) > 0.1f)
				{
					if (!isHeadBobbing)
					{
						headBobEndTween.Kill();
						startHeadBobbingTime = Time.time;
						isHeadBobbing = true;
					}

					var speedMultiplier = Mathf.Max(1f, characterMovement.TargetSpeedMultiplier);
					speedMultiplier *= (characterMovement.IsCrouching ? 1.5f : 1f);
					headBobPositionOffset =
						Mathf.Sin((Time.time - startHeadBobbingTime) * speedMultiplier * (2f * Mathf.PI) *
								  characterSettings.cameraBobbingFrequency) * characterSettings.cameraBobbingAmplitude *
						(characterMovement.IsCrouching ? 0.3f : 1f);

					if (characterMovement.TargetSpeedMultiplier > 1.1f)
						headBobYRotationOffset =
							Mathf.Sin((Time.time - startHeadBobbingTime) * speedMultiplier * (2f * Mathf.PI) *
									  characterSettings.cameraBobbingFrequency * 0.5f) * 0.6f;
					else
						headBobYRotationOffset = 0;
				}
				else if (isHeadBobbing)
				{
					headBobEndTween = DOTween.To(() => headBobPositionOffset, x => headBobPositionOffset = x, 0, 0.5f);
					isHeadBobbing = false;
				}

				headBobXRotationOffset = headBobPositionOffset * 3f;
			}

			// Apply
			cameraTransform.localPosition = new Vector3(0f, headBobPositionOffset + headFallPositionOffset, 0f);
			cameraTransform.localEulerAngles = new Vector3(headBobXRotationOffset, headBobYRotationOffset, 0f);
			headTransform.localRotation = Quaternion.Euler(xRotation, 0f, headTransform.localEulerAngles.z);
			headTransform.localRotation = Quaternion.Lerp(headTransform.localRotation, Quaternion.Euler(headTransform.localEulerAngles.x, 0, targetZRotation), 0.1f * 60f * Time.deltaTime);
			playerBody.localRotation = Quaternion.Euler(0, yaw, 0);
		}

		private void UpdateZoom()
		{
			var targetFov = defaultFov;
			var targetMotionBlurIntensity = 0.5f;
			if (playerInput.GetButton(PlayerInput.Action.Zoom))
			{
				targetFov = 40f;
				targetMotionBlurIntensity = 0.5f;
			}
			else
			{
				if (characterMovement.SpeedMultiplier > 1 && characterController.velocity.magnitude > 0.1f)
				{
					targetMotionBlurIntensity = Mathf.Lerp(0.5f, 1, characterMovement.SpeedMultiplier - 1);
					targetFov = Mathf.Lerp(playerCamera.fieldOfView, defaultFov + 5f, Time.deltaTime);
				}
			}

			motionBlur.intensity.value = Mathf.Lerp(motionBlur.intensity.value, targetMotionBlurIntensity, 0.2f * 60f * Time.deltaTime);
			playerCamera.fieldOfView = Mathf.Lerp(playerCamera.fieldOfView, targetFov, 0.05f * 60f * Time.deltaTime);
		}

		private void CharacterMovementOnFall(float height)
		{
			fallTween.Kill(true);
			fallTween = DOTween.To(() => headFallPositionOffset, x => headFallPositionOffset = x, -height * 0.1f, 0.2f * height).SetEase(characterSettings.fallMovement);
		}
	}
}
